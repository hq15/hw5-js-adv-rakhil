// Асинхронность в Javascript - это выполнение задач в фоновом режиме, что позволяет выполнять следующие по коду задачи, не ожидая завершения предидущих (более ранних по расположению в коде).


const btn = document.querySelector('.btn-ip-search');
btn.addEventListener('click', async function () {
  const userIp = await getIp();
  showIp(userIp);
});

class Position {
  constructor(options) {
    this.ip = options.query;
    this.continent = options.continent;
    this.country = options.country;
    this.regionName = options.regionName;
    this.city = options.city;
  }
}

class PositionDisplay {
  constructor(position) {
    this.position = position;
  }
  addToHtml() {
    const blockInfoPosition = document.createElement('div');
    blockInfoPosition.className = 'position-board';
    document.body.append(blockInfoPosition);
    const stringAddToHtml = `<div class='js-card-position'>
        <h4 class='js-title'>Your IP address: "${this.position.ip}"</h4>
        <p>Continent: "${this.position.continent}"<p/>
        <p>Country: "${this.position.country}"<p/>
        <p>Region name: "${this.position.regionName}"</p>
        <p>City: "${this.position.city}"</p></div>`;
    blockInfoPosition.insertAdjacentHTML('beforeend', `${stringAddToHtml}`);
  }
}

async function showIp(ip) {
  document.querySelector('.position-board').remove();
  const userCard = await showPosition(ip);
  const printableCard = new PositionDisplay(userCard);
  printableCard.addToHtml();
}

async function getIp() {
  const requestURLUsers = `https://api.ipify.org/?format=json`;
  const responseIp = await fetch(requestURLUsers);
  const resultIp = await responseIp.json();
  return resultIp;
}

async function showPosition(userIp) {
  const requestURLIp = `http://ip-api.com/json/${userIp.ip}?fields=continent,country,regionName,city,query`;
  const usersRequest = await fetch(requestURLIp);
  const userPos = await usersRequest.json();
  const userCard = new Position(userPos);
  return userCard;
}
